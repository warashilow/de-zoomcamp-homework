if "transformer" not in globals():
    from mage_ai.data_preparation.decorators import transformer
if "test" not in globals():
    from mage_ai.data_preparation.decorators import test


@transformer
def transform(data, *args, **kwargs):
    data = data[(data["passenger_count"] != 0) & (data["trip_distance"] != 0)].copy()
    data["lpep_pickup_date"] = data["lpep_pickup_datetime"].dt.date

    data.rename(
        columns={
            "VendorID": "vendor_id",
            "RatecodeID": "ratecode_id",
            "PULocationID": "pu_location_id",
            "DOLocationID": "do_location_id",
        },
        inplace=True,
    )

    print(data.shape)

    return data


@test
def test_output(output, *args) -> None:
    """
    Template code for testing the output of the block.
    """
    assert output is not None, "The output is undefined"
    assert output["vendor_id"].isin([1, 2]).all(), "Vendor is undefined"
    assert (
        output["passenger_count"] > 0
    ).any(), "Passenger count must be greater than 0"
    assert (output["trip_distance"] > 0).any(), "Trip distance must be greater than 0"
