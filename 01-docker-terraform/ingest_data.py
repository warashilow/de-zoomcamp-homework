import argparse
import pandas as pd
import time
from sqlalchemy import create_engine


def main(params):
    user = params.user
    password = params.password
    host = params.host
    port = params.port
    db = params.db
    table_name = params.table_name
    url = params.url

    engine = create_engine(f"postgresql://{user}:{password}@{host}:{port}/{db}")

    counter = 0
    for chunk in pd.read_csv(url, chunksize=100000, low_memory=False):
        start_time = time.time()
        counter += 1
        chunk["lpep_pickup_datetime"] = pd.to_datetime(chunk["lpep_pickup_datetime"])
        chunk["lpep_dropoff_datetime"] = pd.to_datetime(chunk["lpep_dropoff_datetime"])

        chunk.to_sql(
            name=table_name, con=engine, if_exists="append", index=False, method="multi"
        )
        end_time = time.time()
        print(
            f"Number of chunk is : {counter}. Processing time of chunk: {(end_time - start_time)}"
        )


if __name__ == "__main__":
    # Parse the command line arguments and calls the main program
    parser = argparse.ArgumentParser(description="Ingest CSV data to Postgres")

    parser.add_argument("--user", help="user name for postgres")
    parser.add_argument("--password", help="password for postgres")
    parser.add_argument("--host", help="host for postgres")
    parser.add_argument("--port", help="port for postgres")
    parser.add_argument("--db", help="database name for postgres")
    parser.add_argument(
        "--table_name", help="name of the table where we will write the results to"
    )
    parser.add_argument("--url", help="url of the csv file")

    args = parser.parse_args()

    main(args)
