-- count records
select
    count(*)
from
    green_taxi_data
where
    lpep_pickup_datetime::date = '2019-09-18' and
    lpep_dropoff_datetime::date = '2019-09-18';

-- Largest trip for each day
select
    *
from
    green_taxi_data
order by
    trip_distance desc;

-- Three biggest pick up Boroughs
select 
    "Borough",
    sum(total_amount)
from 
    green_taxi_data t
join
    zones z
on
    t."PULocationID" = z."LocationID"
where
    lpep_pickup_datetime::date = '2019-09-18' and
    "Borough" != 'Unknown'
group by
    "Borough"
having
    sum(total_amount) > 50000
order by
    1, 2 desc;

-- Largest tip
select 
    *
from
    green_taxi_data t
join
    zones z
on
    t."PULocationID" = z."LocationID"
join
    zones zdo
on
    t."DOLocationID" = zdo."LocationID"
where
    date_trunc('month', lpep_pickup_datetime) = '2019-09-01' and
    z."Zone" = 'Astoria'
order by
    tip_amount desc;
