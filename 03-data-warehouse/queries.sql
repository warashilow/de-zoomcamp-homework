CREATE OR REPLACE EXTERNAL TABLE `nytaxi.external_green_tripdata`
OPTIONS (
  format = 'PARQUET',
  uris = ['gs://<BUCKET_NAME>/green_tripdata_2022-*.parquet']
);

CREATE OR REPLACE TABLE nytaxi.green_tripdata_non_partitoned AS
SELECT * FROM nytaxi.external_green_tripdata;

-- Question 1
select count(1) from `nytaxi.green_tripdata_non_partitoned`;

-- Question 2
select count(distinct PULocationID)
from `nytaxi.external_green_tripdata`;

select count(distinct PULocationID)
from `nytaxi.green_tripdata_non_partitoned`;

-- Question 3
select count(1)
from `nytaxi.green_tripdata_non_partitoned`
where fare_amount = 0;

-- Question 4
CREATE OR REPLACE TABLE nytaxi.green_tripdata_partitoned_clustered
PARTITION BY DATE(lpep_pickup_datetime)
CLUSTER BY PULocationID AS
SELECT * FROM `nytaxi.green_tripdata_non_partitoned`;

select count(distinct PULocationID)
from `nytaxi.green_tripdata_non_partitoned`
where lpep_pickup_datetime between '2022-06-01' and '2022-06-30';

-- Question 5
select count(distinct PULocationID)
from `nytaxi.green_tripdata_partitoned_clustered`
where lpep_pickup_datetime between '2022-06-01' and '2022-06-30';
